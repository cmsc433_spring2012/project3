package cmsc433.p3;

import java.util.LinkedList;
import java.util.List;

/**
 * An efficient single-threaded depth-first solver (Recursive), written such that it hints
 * part of a possible solution.
 */
public class STMazeSolverDFSRec extends SkippingMazeSolver
{
    public STMazeSolverDFSRec(Maze maze)
    {
        super(maze);
    }

    public List<Direction> solve()
    {
        Move firstMove = new Move(this.maze.getStart(), null, null);
        Move lastMove = this.solve(firstMove);

        if (lastMove == null) { return null; }

        LinkedList<Direction> path = new LinkedList<Direction>();
        Move move = lastMove;

        // First, build the path of all moves where we had to make a decision
        // (choice), by jumping from one move to the one which came before it
        while (move.to != null)
        {
            path.addFirst(move.to);
            move = move.previous;
        }

        // Build the full path from only the positions where we made choices
        return this.pathToFullPath(path);
    }

    private Move solve(Move move)
    {
        try
        {
            Choice nextChoice = null;

            if (move.to == null)
            {
                // Where is the first location where I have to make a
                // choice?
                nextChoice = this.firstChoice(move.from);
            }
            else
            {
                // Where is the next location where I have to make a choice,
                // if I go through m.from in the direction of m.to?
                nextChoice = this.follow(move.from, move.to);
            }

            // Explore all choices from this location
            for (Direction next : nextChoice.choices)
            {
                Move nextMove = new Move(nextChoice.at, next, move);
                Move lastMove = this.solve(nextMove);
                if (lastMove != null) { return lastMove; }
            }
        }
        catch (SolutionFound e)
        {
            // This is the last move
            return move;
        }

        return null;
    }
}
